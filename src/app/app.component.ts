import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';

export interface IPost {
  title: string;
  text: string;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [MessageService],
})
export class AppComponent implements OnInit {

  //  Don't Touch
  value1: string;

  public posts: IPost[] = [
    {title: 'New Yourk Times', text: 'tell true'},
    {title: 'Tool Environment', text: 'big data'},
  ];

  constructor(
    public translate: TranslateService
  ) {
    translate.addLangs(['en', 'ua']);
    if (localStorage.getItem('locale')) {
      const browserLang = localStorage.getItem('locale');
      translate.use(browserLang.match(/en|ua/) ? browserLang : 'en');
    } else {
      localStorage.setItem('locale', 'en');
      translate.setDefaultLang('en');
    }
  }
  public room: any;
  ngOnInit() {
  }

  public handleChange(event) {
    console.log(event);
    this.posts.unshift(event)
  }

  changeLang(language: string) {
    localStorage.setItem('locale', language);
    this.translate.use(language);
  }
}
